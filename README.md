<html>
<head>
</head>
<body>

<h1>Arduino için MCP_CAN Kitaplığı</h1>


MCP_CAN kitaplığı v1.5 Bu kitaplık, MCP2515 veya MCP25625 CAN protokol denetleyicisini kullanan herhangi bir kalkan veya kartla uyumludur.

Bu sürüm, protokol denetleyicisinin ID filtre modunun, start() işleviyle saat hızıyla BAUD hızının ayarlanmasını destekler. MCP2515'te 16MHz saat kullanan 5k, 10k, 20k, 50k, 100k, 125k, 250k, 500k ve 1000k baud hızlarının referans olarak bir Peak-System PCAN-USB dongle kullanılarak çalıştığı onaylanmıştır. 8MHz ve 20MHz kristalleri için veri aktarım hızları henüz onaylanmadı ancak uygun şekilde hesaplandı.

readMsgBuf() işlevleri mesaj kimliğini getirir. getCanId() işlevi eskidir ve artık mevcut değildir, onu kullanmayın.

readMsgBuf(*ID, *DLC, *DATA) işlevi, kimlik tipini (genişletilmiş veya standart) döndürür ve uzak istek durum bitini geri getirir.
ID AND 0x80000000 EŞİT 0x80000000 ise, ID Genişletilmiş tiptedir, aksi halde standarttır.
KİMLİK VE 0x40000000 EŞİT 0x40000000 ise, mesaj bir uzak istektir.

readMsgBuf(*ID, *EXT, *DLC, *DATA) işlevi, kimliği değiştirilmeden döndürür ve bize uzak bir istek hakkında bilgi vermez.
EXT true ise, kimlik uzatılır.

sendMsgBuf(ID, DLC, DATA) işlevi, genişletilmiş veya standart kimlikler gönderebilir.
Bir kimliği genişletilmiş olarak işaretlemek için VEYA kimliği 0x80000000 ile işaretleyin.
Uzak bir istek göndermek için VEYA 0x40000000 ile kimlik.

sendMsgBuf(ID, EXT, DLC, DATA), sabit dönüş değerleri dışında değişmedi.

Çizim, setMode() işlevini kullanarak artık protokol denetleyicisini normal çalışmanın yanı sıra uyku moduna, geri döngüye veya yalnızca dinleme modlarına alabilir. Şu anda, begin() işlevi çalıştıktan sonra kod varsayılan olarak geri döngü moduna geçiyor. Etkin bir veri yoluna bağlıyken denetleyici başlatıldığında filtreleme kararlılığını artırmak için bunu buldum.
 
Kullanıcı sırasıyla enOneShotTX() veya disOneShotTX() kullanarak One-Shot iletim modunu çizimden etkinleştirebilir ve devre dışı bırakabilir (varsayılan).

Uyku modundayken CAN veri yolu aktivitesinden uyanmak için setSleepWakeup(1) ile uyandırma kesmesini etkinleştirin. 0'ı geçmek, uyandırma kesintisini (varsayılan) devre dışı bırakacaktır.

<h2>Kurulum</h2>
Bunu "[.../MySketches/]libraries/" klasörüne kopyalayın ve Arduino editörünü yeniden başlatın.

<h2>NOT:</h2> Kitaplığın daha eski bir sürümü varsa (örn. CAN_BUS_Shield), çakışmaları önlemek için onu kitaplıklar klasöründen kaldırdığınızdan veya dosyaları bu kitaplıktakilerle değiştirdiğinizden emin olun.

<h2>İyi çalışmalar!</h2>
 
</body>
</html>   
