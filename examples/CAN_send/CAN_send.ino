#include <mcp_can.h>
#include <SPI.h>

MCP_CAN CAN0(9);     // CS'yi pin 10'a ayarla


void setup()
{
  Serial.begin(115200);

  // MCP2515'i 16 MHz'de 500 kb/s baud hızıyla ve maskeler ve filtreler devre dışı olarak çalıştırarak başlatın.
  if(CAN0.begin(MCP_ANY, CAN_1000KBPS, MCP_16MHZ) == CAN_OK) Serial.println("MCP2515 Başarıyla Başlatıldı!");
  else Serial.println("MCP2515'i Başlatma Hatası...");

  CAN0.setMode(MCP_NORMAL);   // Change to normal mode to allow messages to be transmitted
}

byte data[8] = {0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x70, 0xD0};



void loop()
{
  // send data:  ID = 0x100, Standard CAN Frame, Data length = 8 bytes, 'data' = array of data bytes to send
  // veri gönder: ID = 0x100, Standart CAN Çerçevesi, Veri uzunluğu = 8 bayt, 'veri' = gönderilecek veri bayt dizisi
  byte sndStat = CAN0.sendMsgBuf(0x100, 0, 8, data);
  if(sndStat == CAN_OK){
    Serial.println("Mesaj Başarıyla Gönderildi!");
  } else {
    Serial.println("Mesaj Gönderme Hatası...");
  }
  delay(2000);   // send data per 1000ms
}